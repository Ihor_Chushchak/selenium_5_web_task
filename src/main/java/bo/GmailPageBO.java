package bo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import po.GmailPage;

public class GmailPageBO {

    private static final Logger LOG = LogManager.getLogger(GmailPage.class);
    private GmailPage gmailPage;


    public GmailPageBO(WebDriver driver){
        gmailPage = new GmailPage(driver);
    }

    public void sendEmail(String to,String subject,String message){
        gmailPage.clickComposeEmailButton();
        gmailPage.fillToField(to);
        gmailPage.fillSubjectField(subject);
        gmailPage.fillMessageField(message);
        gmailPage.clickSendEmailButton();
    }

    public void deleteLastEmail(WebDriver driver){
        gmailPage.clickOpenMessageButton();
        if(gmailPage.checkEmailTimeSending(driver)&&gmailPage.checkEmailButton()){
            gmailPage.clickDeleteEmailButton();
        }else { LOG.info("Message is not in sent directory");}
    }

}

