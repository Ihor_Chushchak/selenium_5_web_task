package bo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import po.AuthorizationPage;

public class AuthorizationPageBO {
    private static final Logger LOG = LogManager.getLogger(AuthorizationPage.class);
    private AuthorizationPage authorizationPage;

    public AuthorizationPageBO(WebDriver driver){
        authorizationPage = new AuthorizationPage(driver);
    }

    public void enterLoginToGmailPage(String login){
        authorizationPage.fillLogin(login);
        authorizationPage.clickLoginNextButton();
    }

    public void enterPasswordToGmailPage(String password,WebDriver driver, WebDriverWait wait){
        authorizationPage.fillPassword(password);
        authorizationPage.clickPasswordNextButton(driver,wait);
    }
}
