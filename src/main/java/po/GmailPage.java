package po;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class GmailPage {

    private static final Logger LOG = LogManager.getLogger(GmailPage.class);

    @FindBy(css = "div.aic div[role='button']")
    private WebElement composeEmailButton;

    @FindBy(css = "textarea[name='to']")
    private WebElement toField;

    @FindBy(css = "input[name='subjectbox']")
    private WebElement subjectField;

    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private WebElement messageField;

    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private WebElement sendEmailButton;

    @FindBy(xpath = "//*[@id='link_vsm']")
    private WebElement openMessageButton;

    @FindBy(xpath = "//*[@id=\":j7\"]/div/div[2]/span/a")
    private WebElement chooseSendButton;

    @FindBy(xpath = "//*[@id=\":4\"]/div[2]/div[1]/div/div[2]/div[3]")
    private WebElement deleteEmailButton;

    public GmailPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void clickComposeEmailButton() {
        LOG.info("Clicking compose_email button");
        composeEmailButton.click();
    }

    public void fillToField(String to) {
        LOG.info("Filling \"To\" filed");
        toField.sendKeys(to);
    }

    public void fillSubjectField(String subject) {
        LOG.info("Filling \"Subject\" filed");
        subjectField.sendKeys(subject);
    }

    public void fillMessageField(String message) {
        LOG.info("Filling \"Message\" filed");
        messageField.sendKeys(message);
    }

    public void clickSendEmailButton() {
        LOG.info("Clicking send_email button");
        sendEmailButton.click();
    }

    public void clickOpenMessageButton() {
        LOG.info("Clicking open_sended_email button");
        openMessageButton.click();
    }

    public String getSendButtonAttribute(){
     return chooseSendButton.getAttribute("tabindex");
    }

    public void clickDeleteEmailButton(){
        LOG.info("Clicking delete_sended_email button");
        deleteEmailButton.click();
    }

    public boolean checkEmailTimeSending(WebDriver driver) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime localDateTime = LocalDateTime.now();
        String actualTime = dateTimeFormatter.format(localDateTime) + " (0 хвилин тому)";
        return Objects.equals(actualTime,driver.findElement(By.xpath("//span[@class='g3'][@role='gridcell']")).getText());

    }

    public boolean checkEmailButton(){
        return Objects.equals(getSendButtonAttribute(),"0");
    }
}
