package po;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AuthorizationPage {

    private static final Logger LOG = LogManager.getLogger(AuthorizationPage.class);

    @FindBy(css = "#identifierId")
    private WebElement loginInput;

    @FindBy(xpath = "//*[@id=\"identifierNext\"]/span")
    private WebElement loginNextButton;

    @FindBy(xpath = "//*[@id=\"password\"]/div[1]/div/div[1]/input")
    private WebElement passwordInput;

    @FindBy(css = "#passwordNext")
    private WebElement passwordNextButton;

    public AuthorizationPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void fillLogin(String login) {
        LOG.info("Entering login");
        loginInput.sendKeys(login);
    }

    public void clickLoginNextButton() {
        LOG.info("Moving to password_submit page");
        loginNextButton.click();
    }

    public void fillPassword(String password) {
        LOG.info("Entering password");
        passwordInput.sendKeys(password);
    }

    public void clickPasswordNextButton(WebDriver driver, WebDriverWait wait) {
        LOG.info("Moving to gmail_main page");
        wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(passwordNextButton));
        passwordNextButton.click();
    }
}
