package po;

import bo.AuthorizationPageBO;
import bo.GmailPageBO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.Iterator;
import java.util.stream.Stream;

import utils.ConfigReader;
import utils.DriverManager;

import static org.testng.Assert.assertEquals;

public class GmailPageTest {

    private static WebDriver driver;
    private static  WebDriverWait wait;

    private static final Logger LOG = LogManager.getLogger(GmailPageTest.class);

    @DataProvider(parallel = true)
    public Iterator<Object[]> users(){
        return Stream.of(
                new Object[]{ConfigReader.read("EMAIL_LOGIN"),ConfigReader.read("EMAIL_PASSWORD")},
                new Object[]{ConfigReader.read("EMAIL_LOGIN"),ConfigReader.read("EMAIL_PASSWORD")}).iterator();
//                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_2"),ConfigReader.read("SOME_EMAIL_PASSWORD_2")},
//                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_3"),ConfigReader.read("SOME_EMAIL_PASSWORD_3")},
//                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_4"),ConfigReader.read("SOME_EMAIL_PASSWORD_4")},
//                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_5"),ConfigReader.read("SOME_EMAIL_PASSWORD_5")}).iterator();
    }

    @BeforeMethod
    static void initializeObjects() {
        LOG.info("Open web-driver");
        DriverManager.getDriver().get(ConfigReader.read("HOME_PAGE"));
        LOG.info("Moving to login_submit page");
        driver = DriverManager.getDriver();
    }

    @Test(dataProvider = "users")
    void openGmailAndLoginTest(String userEmail, String password){
        AuthorizationPageBO authorizationPageBO = new AuthorizationPageBO(driver);
        authorizationPageBO.enterLoginToGmailPage(userEmail);
        authorizationPageBO.enterPasswordToGmailPage(password,driver,wait);
        assertEquals( driver.findElement(By.cssSelector("div.aic div[role='button']")).getText(),"Написати");
        GmailPageBO gmailPageBO = new GmailPageBO(driver);
        gmailPageBO.sendEmail(userEmail,ConfigReader.read("EMAIL_SUBJECT"),ConfigReader.read("EMAIL_MESSAGE"));
        gmailPageBO.deleteLastEmail(driver);
        assertEquals(driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[1]/div" +
                "[4]/div[1]/div/div[3]/div/div/div[2]" +
                "/span/span[1]")).getText(),"Ланцюжок повідомлень переміщено в кошик.");
    }

    @AfterMethod
    static void closeResources() {
        driver.quit();
        DriverManager.closeResources();
    }

}


